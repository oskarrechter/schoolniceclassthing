﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchoolLibrary;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class KlasseGenerator
    {
        private static readonly Random R = new Random();

        private static List<Student> GetRandomStudents(ref List<Student> students)
        {
            var studentList = new List<Student>();
            for (var i = 0; i < R.Next(0, 100); i++)
                studentList.Add(students.ElementAt(R.Next(0, students.Count - 1)));
            return studentList;
        }

        private static List<Course> GetRandomCourses(ref List<Course> courses)
        {
            var courseList = new List<Course>();
            for (var i = 0; i < R.Next(0, 100); i++)
                courseList.Add(courses.ElementAt(R.Next(0, courses.Count)));
            return courseList;
        }

        private static string GetName()
        {
            const string numbers = "0123456789";
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

            var result = "";
            result += numbers[R.Next(0, numbers.Length)];
            result += ".";
            result += chars[R.Next(0, chars.Length)];

            return result;
        }

        public static List<Klasse> GenerateClasses(int amount, ref List<Student> students, ref List<Course> courses)
        {
            var classes = new List<Klasse>();
            for (var i = 0; i < amount; i++)
            {
                var name = GetName();
                var studentList = GetRandomStudents(ref students);
                var courseList = GetRandomCourses(ref courses);

                classes.Add(new Klasse(name, studentList, courseList));
            }
            return classes;
        }
    }
}
