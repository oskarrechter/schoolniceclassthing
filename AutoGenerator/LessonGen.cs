﻿using System;
using System.Collections.Generic;
using SchoolLibrary;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class LessonGen
    {
        public static IEnumerable<Lesson> LessonGenerator(int count, ref List<Room> rooms, ref List<Course> courses, ref List<Klasse> klasser)
        {
            var lessons = new List<Lesson>();
            var rand = new Random();
            for (var i = 0; i < count; i++)
            {
                var lesson = new Lesson
                {
                    Course = courses[rand.Next(0, courses.Count - 1)],
                    Room = rooms[rand.Next(0, rooms.Count - 1)],
                    StartTime = DateTime.Now.AddSeconds(rand.Next(31560000))
                };
                lesson.EndTime = lesson.StartTime.AddSeconds(rand.Next(10800));
                lesson.Klasse = klasser[rand.Next(klasser.Count - 1)];
                lessons.Add(lesson);
            }
            return lessons;
        }
    }
}