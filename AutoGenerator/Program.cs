﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using SchoolLibrary;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class Program
    {
        private static void Main(string[] args)
        {
            var r = new Random();
            var dir = "";
            if (File.Exists("setup.ini"))
            {
                var lines = File.ReadAllLines("setup.ini");
                var temp = lines[0].Split('=');
                dir = temp[1];
            }
            else
            {
                Console.WriteLine("Please enter folder path: ");
                var dirtemp = Console.ReadLine();

                var temp = "";
                if (dirtemp != null)
                    foreach (var t in dirtemp)
                    {
                        if (t == '\\')
                        {
                            temp += "\\";
                            temp += "\\";
                        }
                        else
                            temp += t;
                    }

                using (var writer = new StreamWriter("setup.ini"))
                {
                    writer.WriteLine("dir=" + temp);
                }

                dir = temp;
            }
            // ReSharper disable once ConditionIsAlwaysTrueOrFalse
            if (Directory.Exists(dir + '\\' + "Schools") && Directory.GetFiles(dir + '\\' + "Schools") != null)
            {
                var files = Directory.GetFiles(dir + '\\' + "Schools");
                
                foreach (var file in files)
                {
                    var school = JsonConvert.DeserializeObject<School>(File.ReadAllText(file));
                    if (school != null) Console.WriteLine(school.ToString());
                }
            }
            else
            {
                Console.WriteLine("Please enter Amount to number generate");
                var amount = Convert.ToInt16(Console.ReadLine());
                
                var people = StudentGen.StudentGenerator(amount*2, "LOLNOOB");
                Console.WriteLine("TEST1");
                var staff = StaffGen.StaffGenerator(amount*2, "LORt");
                Console.WriteLine("TEST2");
                var room = RoomGenerator.GenerateRooms(amount*2);
                Console.WriteLine("TEST3");
                var course = CourseAutoGenerator.GenerateCourses(ref staff, amount*2);
                Course.Save(course, dir);
                Console.WriteLine("TEST4");
                var klasse = KlasseGenerator.GenerateClasses(amount*2, ref people, ref course);
                Console.WriteLine("TEST5");
                var lessons = LessonGen.LessonGenerator(amount*2, ref room, ref course, ref klasse);
                Console.WriteLine("TEST6");
                var school = SchoolGenerator.GenerateSchools(amount, ref people, ref staff, ref course, ref klasse, ref room);
                School.Save(school as List<School>, dir);
                Console.WriteLine("TEST7");
            }
            Console.ReadLine();
        }
    }
}
