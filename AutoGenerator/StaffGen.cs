﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SchoolLibrary;
using Newtonsoft.Json;
using System.IO;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class StaffGen
    {
        public static List<Staff> StaffGenerator(int count, string schoolname)
        {
            var staff = new List<Staff>();
            var fornavne = JsonConvert.DeserializeObject<List<Fornavn>>(File.ReadAllText("fornavne.json"));
            var efternavne = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText("efternavne.json"));
            var vejnavne = JsonConvert.DeserializeObject<List<string>>(File.ReadAllText("vejnavne.json"));
            var byer = JsonConvert.DeserializeObject<List<By>>(File.ReadAllText("zipCodes.json"));
            char[] vejnummerchar = { ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J' };
            var random = new Random();
            for (var i = 0; i < count; i++)
            {
                if (efternavne == null) continue;
                var efternavn = efternavne[random.Next(0, efternavne.Count - 1)];
                    
                if (fornavne == null) continue;
                var name = fornavne[random.Next(0, fornavne.Count - 1)].Navn + " " + efternavn;
                        
                var birthday = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(random.Next(315529200, 1577833200));
                    
                if (byer == null) continue;
                var zipcode = byer[random.Next(0, byer.Count - 1)].ZipCode;
                if (vejnavne == null) continue;
                var vejnavn = vejnavne[random.Next(0, vejnavne.Count - 1)];
                var bynavn = byer[random.Next(0, byer.Count - 1)].City;
                var vejnummer = random.Next(1, 300).ToString() + vejnummerchar[random.Next(1, vejnummerchar.Length - 1)];
                    
                var address = new Adresse(zipcode, vejnavn, bynavn, vejnummer);
                    
                var number = random.Next(0, 99999999).ToString();
                if (number.Length < 8)
                    for (var b = number.Length; b < 8; b++)
                        number = "0" + number;
                    
                var phoneNumber = "+45" + number;
                var email = efternavn + "@" + schoolname.Replace(" ", ".") + ".com";
                var height = (ushort)random.Next(80, 210);
                var accessLevel = (ushort)random.Next(0, 2);
                    
                var staffmember = new Staff(name, birthday, address, phoneNumber, email, height, random.Next(0, 1).ToString(), accessLevel);
                    
                staff.Add(staffmember);
            }
            return staff;
        }
    }
}
