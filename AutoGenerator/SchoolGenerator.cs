﻿using System;
using System.Collections.Generic;
using System.Linq;
using SchoolLibrary;
using Newtonsoft.Json;
using System.IO;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace AutoGenerator
{
    internal static class SchoolGenerator
    {
        private static readonly Random R = new Random();

        private static List<Student> GetSublistStudents(ref List<Student> students)
        {
            var student = new List<Student>();

            for (var j = 0; j < R.Next(0, 100); j++)
                student.Add(students.ElementAt(R.Next(0, students.Count)));

            return student;
        }

        private static List<Staff> GetSublistStaff(ref List<Staff> staff)
        {
            var staffList = new List<Staff>();

            for (var j = 0; j < R.Next(0, 100); j++)
                staffList.Add(staff.ElementAt(R.Next(0, staff.Count)));

            return staffList;
        }

        private static List<Course> GetSublistCourse(ref List<Course> courses)
        {
            var courseList = new List<Course>();

            for (var j = 0; j < R.Next(0, 100); j++)
                courseList.Add(courses.ElementAt(R.Next(0, courses.Count)));

            return courseList;
        }

        private static List<Klasse> GetSublistClasses(ref List<Klasse> classes)
        {
            var classList = new List<Klasse>();

            for (var j = 0; j < R.Next(0, 100); j++)
                classList.Add(classes.ElementAt(R.Next(0, classes.Count)));

            return classList;
        }

        private static List<Room> GetSublistRooms(ref List<Room> rooms)
        {
            var roomList = new List<Room>();

            for (var j = 0; j < R.Next(0, 100); j++)
                roomList.Add(rooms.ElementAt(R.Next(0, rooms.Count)));

            return roomList;
        }

        public static IEnumerable<School> GenerateSchools(int amount, ref List<Student> students, ref List<Staff> staff, ref List<Course> courses, ref List<Klasse> classes, ref List<Room> rooms)
        {
            var schools = new List<School>();
            var byer = JsonConvert.DeserializeObject<List<By>>(File.ReadAllText("zipcodes.json"));
            if (byer == null) return schools;
            var names = byer.Select(@by => @by.City).ToList();

            // For loop for creating new schools
            for (var i = 0; i < amount; i++)
            {
                var name = names.ElementAt(R.Next(0, names.Count));

                var studentList = GetSublistStudents(ref students);
                var staffList = GetSublistStaff(ref staff);
                var courseList = GetSublistCourse(ref courses);
                var classList = GetSublistClasses(ref classes);
                var roomList = GetSublistRooms(ref rooms);

                var school = new School(name, studentList, staffList, courseList, classList, roomList);

                schools.Add(school);
            }

            return schools;
        }
    }
}
