﻿using System.Collections.Generic;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global

namespace SchoolLibrary
{
    public class School
    {
        public string Name { get; set; }
        public List<Student> Students { get; set; }
        public List<Staff> Staffs { get; set; }
        public List<Course> Courses { get; set; }
        public List<Klasse> Classes { get; set; }
        public List<Room> Rooms { get; set; }

        public School(string name, List<Student> students, List<Staff> staffs, List<Course> courses, List<Klasse> classes, List<Room> rooms)
        {
            this.Name = name;
            this.Students = students;
            this.Staffs = staffs;
            this.Courses = courses;
            this.Classes = classes;
            this.Rooms = rooms;
        }

        private void CreateClass(string name, List<Student> students, List<Course> courses)
        {
            var klasse = new Klasse(name, students, courses);
            Classes.Add(klasse);
        }

        private void CreateNewStudent(string name,
                              DateTime birthday,
                              Adresse address,
                              string phoneNumber,
                              string email,
                              ushort height,
                              ushort accessLevel,
                              List<Klasse> classes)
        {
            var student = new Student(name, birthday, address,
                                      phoneNumber, email, height,
                                      accessLevel, classes);
            Students.Add(student);
        }

        private void CreateNewStaff(string name,
                            DateTime birthday,
                            Adresse address,
                            string phoneNumber,
                            string email,
                            ushort height,
                            string position,
                            ushort accessLevel)
        {
            var newStaff = new Staff(name, birthday, address, 
                                       phoneNumber, email, height, 
                                       position, accessLevel);
            Staffs.Add(newStaff);
        }

        private string GetHash()
        {
            var hash = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(Name + Students + Staffs + Courses + Classes + Rooms));

            var sOutput = new StringBuilder(hash.Length);
            foreach (var t in hash)
            {
                sOutput.Append(t.ToString("X2"));
            }

            return sOutput.ToString();
        }

        private static Dictionary<string, School> _schools;
        private const string CourseDir = "Schools";

        private static ref Dictionary<string, School> GetList()
        {
            if (_schools != null) return ref _schools;
            _schools = new Dictionary<string, School>();

            var dirPath = Setup.GetSetup().DataPath + CourseDir;
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            foreach (var file in Directory.GetFiles(dirPath))
            {
                var add = Newtonsoft.Json.JsonConvert.DeserializeObject<School>(File.ReadAllText(file));
                if (add != null) _schools.Add(add.GetHash(), add);
            }

            return ref _schools;
        }
        public static School Find(string key)
        {
            if (_schools == null)
            {
                GetList();
            }

            if (_schools != null && _schools.TryGetValue(key, out var add))
            {
                return add;
            }

            return null;
        }
        
        public static void Save(IEnumerable<School> schoolList, string path)
        {
            var dirPath = path + '\\' + CourseDir;
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            foreach (var school in schoolList)
            {
                using (var writer = new StreamWriter(dirPath + "\\" + school.GetHash() + ".json"))
                {
                    writer.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(school));
                }
            }
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
