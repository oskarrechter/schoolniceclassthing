﻿// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Room
    {
        
        public string Name { get; set; }

        public uint Number { get; set; }

        public Room(string name, uint number)
        {
            this.Name = name;
            this.Number = number;
        }
    }
}