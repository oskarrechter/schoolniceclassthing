﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.IO;
using Newtonsoft;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
namespace SchoolLibrary
{
    public class Course
    {
        public string Name { get; set; }
        public string Subject { get; set; }
        public Staff Teachers { get; set; }
        public int AmountOfLessons { get; set; }

        public string Description { get; set; }

        private string GetHash()
        {
            var hash = new MD5CryptoServiceProvider().ComputeHash(Encoding.UTF8.GetBytes(Name + Subject + Teachers + AmountOfLessons + Description));

            var sOutput = new StringBuilder(hash.Length);
            foreach (var t in hash)
            {
                sOutput.Append(t.ToString("X2"));
            }

            return sOutput.ToString();
        }

        private const string CourseDir = "Courses";
        private static Dictionary<string, Course> _courses = null;

        private static ref Dictionary<string, Course> GetList()
        {
            if (_courses != null) return ref _courses;
            _courses = new Dictionary<string, Course>();

            var dirPath = Setup.GetSetup().DataPath + CourseDir;
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            foreach (var file in Directory.GetFiles(dirPath))
            {
                var add = Newtonsoft.Json.JsonConvert.DeserializeObject<Course>(File.ReadAllText(file));
                if (add != null) _courses.Add(add.GetHash(), add);
            }

            return ref _courses;
        }

        public static Course Find(string key)
        {
            if (_courses == null)
            {
                GetList();
            }

            if (_courses != null && _courses.TryGetValue(key, out var add))
            {
                return add;
            }

            return null;
        }

        public static void Save(List<Course> courses, string path)
        {
            var dirPath = path + '\\' + CourseDir;
            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            foreach (var course in from course in courses let aH = course.GetHash() select course)
            {
                using (var writer = new StreamWriter(dirPath + "\\" + course.GetHash() + ".json"))
                {
                    writer.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(course));
                }
            }
        }

        public override string ToString()
        {
            var output = new StringBuilder();
            output.AppendLine("Navn og emne : " + Name + " " + Subject);
            output.AppendLine("Lærere og antal lektioner : " + Teachers + " " + AmountOfLessons);
            output.AppendLine("Beskrivelse); " + Description);

            return output.ToString();
        }
    }

}