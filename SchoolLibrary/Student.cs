﻿using System.Collections.Generic;
using System;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable global MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Student : Person
    {
        private List<Klasse> Classes { get; set; }

        private List<Grade> Grades { get; set; } = new List<Grade>();

        public Student(string name,
            DateTime birthday,
            Adresse address,
            string phoneNumber,
            string email,
            ushort height,
            ushort accessLevel,
            List<Klasse> classes) 
            : base(name,
                   birthday,
                   address,
                   phoneNumber,
                   email,
                   height,
                   accessLevel)
        {
            this.Classes = classes;
        }
    }
}