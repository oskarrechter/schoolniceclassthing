﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Person
    {
        public string Name { get; set; }
        
        private DateTime Birthday { get; set; }
        
        public Adresse Address { get; set; }
        
        private string PhoneNumber { get; set; }
        
        public string Email { get; set; }

        private ushort Height { get; set; }
        
        private ushort AccessLevel { get; set; }

        protected Person(string name, 
            DateTime birthday, 
            Adresse address, 
            string phoneNumber, 
            string email, 
            ushort height,
            ushort accessLevel)
        {
            this.Name = name;
            this.Birthday = birthday;
            this.Address = address;
            this.PhoneNumber = phoneNumber;
            this.Email = email;
            this.Height = height;
            this.AccessLevel = accessLevel;
        }
    }
}
