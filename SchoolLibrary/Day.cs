﻿using System;

namespace SchoolLibrary
{
    public class Day
    {
        public DateTime Date { get; set; }
        public Lesson[] Lessons { get; set; } = new Lesson[5000];
    }
}
