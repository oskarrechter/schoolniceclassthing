﻿using System;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public abstract class Grade
    {
        private Course Course { get; set; }

        
        public string Mark { get; set; }
        
        private DateTime Date { get; set; }

        protected Grade(Course course, string mark, DateTime date) {
            this.Course = course;
            this.Mark = mark;
            this.Date = date;
        }
    }
}
