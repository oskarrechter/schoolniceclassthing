﻿using System.Collections.Generic;
// ReSharper disable global IdentifierTypo
// ReSharper disable global StringLiteralTypo
// ReSharper disable global CommentTypo
// ReSharper disable global UnusedAutoPropertyAccessor.Global
// ReSharper disable global UnusedAutoPropertyAccessor.Local
// ReSharper disable once MemberCanBePrivate.Global
namespace SchoolLibrary
{
    public class Klasse
    {
        public string Name { get; set; }

        private List<Student> Students { get; set; }
        private List<Course> Courses { get; set; }

        public Klasse(string name, List<Student> students, List<Course> courses)
        {
            this.Name = name;
            this.Students = students;
            this.Courses = courses;
        }
    }
}